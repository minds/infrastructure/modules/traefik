# traefik

| Node Group | Deployment | Environments | Namespace  |
| ---------- | ---------- | ------------ | ---------- |
| all  | Argo CD    | Sandbox, Production   | traefik |

## Description

Helm chart for Traefik. Used as the Ingress Controller for Minds K8s clusters.

## Deploy

Deployed using Argo CD. View the application in Argo, and "Sync" when changes are in master.
